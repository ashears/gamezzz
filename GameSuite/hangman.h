#ifndef HANGMAN_H
#define HANGMAN_H
#include <QList>
#include <QLabel>
#include <QMainWindow>
#include <QObject>
namespace Ui {
class Hangman;
}

class Hangman : public QMainWindow
{
    Q_OBJECT

public:
    explicit Hangman(QWidget *parent = 0);
    ~Hangman();

private slots:

    void on_pushButton_submit_clicked();

    void on_pushButton_exit_clicked();

private:
    Ui::Hangman *ui;
    int wrong_answers;
    int right_answers;
    QString path;
    QString current_path;
    QString hang_state;
    std::vector<QString> word_list;
    std::vector<QString> shuffled_list;
    QString current_word;
    int check_letter(QString input_text, QCharRef correct_ans);
    int check_input(QString input);
    bool found_letter;
    QList<QLabel *> visible_word;
    QString upper_input;
    int check_Used_Letters(QString letter);
    void append_used_letter(QString letter);
    void toggleGameEnd(QString result);



};

#endif // HANGMAN_H
