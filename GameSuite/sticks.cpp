#include "sticks.h"
#include "ui_sticks.h"
#include "login.h"
Sticks::Sticks(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Sticks)
{
    ui->setupUi(this);
    QPixmap pix(":/resource/img/duck_line.jpg");//Takes location of image as arg
    ui->label_pic->setPixmap(pix);
}

Sticks::~Sticks()
{
    delete ui;
}

void Sticks::selectUserChoice(int choice){
    ui->pushButton_select1->setEnabled(false);
    ui->pushButton_select2->setEnabled(false);
    ui->pushButton_select3->setEnabled(false);
    numsticks = numsticks - choice;
    setMessage("You grabbed " + QString::number(choice) + " sticks. " + QString::number(numsticks) + " sticks remaining");
    if (numsticks == 0){
        gameEnd("Lose");
        return;
    }
    computerChoice = getComputerChoice(numsticks);
    numsticks = numsticks - computerChoice;
    setMessage("Computer grabbed " + QString::number(computerChoice) + " sticks. " + QString::number(numsticks) + " sticks remaining");
    if (numsticks == 0){
        gameEnd("Win");
        return;
    }
    if (numsticks == 1)
        ui->pushButton_select1->setEnabled(true);
    else if (numsticks == 2){
        ui->pushButton_select1->setEnabled(true);
        ui->pushButton_select2->setEnabled(true);
    }
    else{
        ui->pushButton_select1->setEnabled(true);
        ui->pushButton_select2->setEnabled(true);
        ui->pushButton_select3->setEnabled(true);
    }
}

int Sticks::getComputerChoice(int current_sticks){
    /*Get a pseudo-random integer between 1 and 3 (inclusive)*/
    int choice = rand() % 3 + 1;

    if (current_sticks >=2 && current_sticks <=4){
        choice = current_sticks - 1;
    }
    else if (current_sticks == 1){
        return current_sticks;
    }
    return choice;
}

void Sticks::gameEnd(QString result){
    if (result == "Win"){
        togglePushButtons("FALSE");
        setMessage("Congrats, you are a winner! I knew you could do it.");
    }
    else if (result == "Lose"){
        togglePushButtons("FALSE");
        setMessage("You are a loser! Maybe next time.");
    }
}

void Sticks::on_pushButton_select1_clicked()
{
    selectUserChoice(1);
}

void Sticks::on_pushButton_select2_clicked()
{
    selectUserChoice(2);
}

void Sticks::on_pushButton_select3_clicked()
{
    selectUserChoice(3);
}

void Sticks::on_pushButton_select_num_sticks_clicked()
{
    numsticks = ui->lineEdit_numsticks->text().toInt();
    if (numsticks < 10 || numsticks > 100) //Check that numsticks is 10-100
        return;
    setMessage("There are " + QString::number(numsticks) + " sticks remaining.");
    ui->label_current_sticks->hide();
    ui->pushButton_select_num_sticks->hide();
    ui->lineEdit_numsticks->hide();
    togglePushButtons("TRUE");
}

void Sticks::togglePushButtons(QString input){
    if (input == "TRUE"){
        ui->pushButton_select1->setEnabled(true);
        ui->pushButton_select2->setEnabled(true);
        ui->pushButton_select3->setEnabled(true);
        ui->label_grab->setEnabled(true);
    }
    else if (input == "FALSE"){
        ui->pushButton_select1->setEnabled(false);
        ui->pushButton_select2->setEnabled(false);
        ui->pushButton_select3->setEnabled(false);
        ui->label_grab->setEnabled(false);
    }
}

void Sticks::setMessage(QString msg){
    ui->label_msg4->setText(ui->label_msg3->text());
    ui->label_msg3->setText(ui->label_msg2->text());
    ui->label_msg2->setText(ui->label_msg1->text());
    ui->label_msg1->setText(msg);
}


void Sticks::on_pushButton_exit_clicked()
{
    parentWidget()->show();
    hide();
    deleteLater();
}
