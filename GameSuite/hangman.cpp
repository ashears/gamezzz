#include "hangman.h"
#include "ui_hangman.h"
#include <QDebug>


Hangman::Hangman(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Hangman)
{
    ui->setupUi(this);

    word_list = {"BANANAS", "JUKEBOX", "GRIZZLY", "ENZYMIC", "QUACKED", "JUNKIES", "JACUZZI", "SQUIRMY", "ADJUNCT",
                 "STEREOS", "TOSTADA", "ETERNAL", "CUTBACK", "PREQUEL", "SUCCUMB", "BOUQUET", "BANQUET", "SQUISHY",
                 "CHUCKLE", "BEESWAX", "BICYCLE", "EXHIBIT", "PSYCHIC", "EXCERPT", "AQUATIC", "BULWARK", "HEXAGON"};
    std::random_shuffle(word_list.begin(), word_list.end());
    wrong_answers = 0;
    right_answers = 0;
    current_word = word_list.front();
    visible_word = {ui->label_0, ui->label_1,ui->label_2, ui->label_3,ui->label_4, ui->label_5, ui->label_6};

    /*Sets up initial hangman image path*/
    path = ":/resource/hangman_pics/hang";
    hang_state = QString::number(wrong_answers);
    current_path = path + hang_state + ".png";

    /*Sets up hangman image*/
    QPixmap pix(current_path);//Takes location of image as arg
    int w = ui->label_pic->width();
    int h = ui->label_pic->height();
    ui->label_pic->setPixmap(pix.scaled(w,h,Qt::KeepAspectRatio));
}

Hangman::~Hangman()
{
    delete ui;
}

/*Checks input and if input is correct places input on the game board*/
int Hangman::check_input(QString input){
    upper_input = input.toUpper(); //Converts to upper to avoid discrepencies between lower and upper case input
    found_letter = false;
    if (check_Used_Letters(upper_input) == 1)
        return 1; //If have used letter, exit function early.
    int i;
    for (i = 0; i < 7; i++){ //All words have seven elements
        if (upper_input == current_word[i]){
            visible_word[i]->setText(upper_input); //Place user input on game view.
            found_letter = true;
            right_answers++;
            if (right_answers == 7)
                toggleGameEnd("Win");
        }
        /*If letter has not been used, add to list of used letters*/
        if (check_Used_Letters(upper_input) == 0){ //This check is here so no multiple letters are added to used list
            append_used_letter(upper_input);

        }
    }
    if (found_letter == true)
        return 1; //Found letter
    else
        return 0; //Did not find letter
}
/*Appends used letters to a label*/
void Hangman::append_used_letter(QString letter){
    QString current_text = ui->label_used_letter_list->text();
    ui->label_used_letter_list->setText(current_text + letter);
}

int Hangman::check_Used_Letters(QString letter){
    int j;
    for (j = 0; j < ui->label_used_letter_list->text().length(); j++){
        if (ui->label_used_letter_list->text()[j] == letter){
            ui->statusbar->showMessage(letter + " has been used", 2000);
            return 1;
        }
    }
    return 0;
}

void Hangman::on_pushButton_submit_clicked()
{

    int checked_input = check_input(ui->input->text());
    if (checked_input == 0){ //Checks whether user input is an element of the current word
        /*If input is not an element of current word, bring up next hangman image*/
        wrong_answers++;
        hang_state = QString::number(wrong_answers);
        current_path = path + hang_state + ".png";
        QPixmap pix(current_path);
        int w = ui->label_pic->width();
        int h = ui->label_pic->height();
        ui->label_pic->setPixmap(pix.scaled(w,h,Qt::KeepAspectRatio));
        if (wrong_answers == 6)
            toggleGameEnd("Loss");
    }
    ui->input->setText("");
}

void Hangman::toggleGameEnd(QString result){
    ui->pushButton_submit->setDisabled(true);
    ui->input->setDisabled(true);
    if (result == "Loss")
        ui->label_result->setText("Gameover. You lose!");
    if (result == "Win")
        ui->label_result->setText("Gameover. You are a winner!");
}

void Hangman::on_pushButton_exit_clicked()
{
    parentWidget()->show();
    hide();
    deleteLater();
}
