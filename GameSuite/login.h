#ifndef LOGIN_H
#define LOGIN_H

#include <QMainWindow>
#include <QtSql>
#include <QFileInfo>
#include "gameselectwindow.h"
#include "createuser.h"
#include "dbmanager.h"
namespace Ui {
class Login;
}

class Login : public QMainWindow
{
    Q_OBJECT

public:
    explicit Login(QWidget *parent = 0);
    GameSelectWindow *gameSelectWindow;
    ~Login();

private slots:
    void on_pushButton_login_clicked();

    void on_pushButton_createuser_clicked();

private:
    QString path;
    Ui::Login *ui;
    QSqlDatabase mydb;

    CreateUser *createUser;
    DBManager dbmanager;
    QString current_user;
};

#endif // LOGIN_H
