#include "gameselectwindow.h"
#include "ui_gameselectwindow.h"

GameSelectWindow::GameSelectWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::GameSelectWindow)
{
    ui->setupUi(this);
    QPixmap pix(":/resource/img/duckpic2.jpg");//Takes location of image as arg
    int w = ui->label_pic->width();
    int h = ui->label_pic->height();
    ui->label_pic->setPixmap(pix.scaled(w,h,Qt::KeepAspectRatio));
}

GameSelectWindow::~GameSelectWindow()
{
    delete ui;
}

void GameSelectWindow::on_pushButton_3_clicked()
{
    hide(); //Hides GameSelect page
    hangman = new Hangman(this);
    hangman->show();
}

void GameSelectWindow::on_pushButton_sticks_clicked()
{
    hide(); //Hides GameSelect page

    sticks = new Sticks(this);
    sticks->show();
}



void GameSelectWindow::on_pushButton_tictactoe_clicked()
{
    hide();
    ttt = new TicTacToe(this);
    ttt->show();
}

void GameSelectWindow::on_pushButton_clicked()
{
    hide();
    game = new twenty_48(this);
    game->initializeBoard();
    game->update();
    QWidget *g = game;
    //game->current_user = current_user;
    g->show();
}
