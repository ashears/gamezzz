#ifndef STICKS_H
#define STICKS_H

#include <QMainWindow>
#include <sstream>
namespace Ui {
class Sticks;
}

class Sticks : public QMainWindow
{
    Q_OBJECT

public:
    explicit Sticks(QWidget *parent = 0);
    ~Sticks();

private slots:
    void on_pushButton_select1_clicked();

    void on_pushButton_select2_clicked();

    void on_pushButton_select3_clicked();

    void on_pushButton_select_num_sticks_clicked();

    void on_pushButton_exit_clicked();

private:
    Ui::Sticks *ui;
    void selectUserChoice(int i);
    int getComputerChoice(int i);
    void gameEnd(QString str);
    void setMessage(QString str);
    void togglePushButtons(QString input);
    QString num_stick_msg;
    int numsticks;
    std::stringstream ss;
    int computerChoice;
};

#endif // STICKS_H
