#include "tictactoe.h"
#include "ui_tictactoe.h"
#include "QDebug"
TicTacToe::TicTacToe(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::TicTacToe)
{
    ui->setupUi(this);
    gameover = 0;
}

TicTacToe::~TicTacToe()
{
    delete ui;
}
void TicTacToe::check_board(){
    /*Check for horizontal winner*/
    if (ui->pushButton_00->text() == ui->pushButton_01->text() && ui->pushButton_01->text() == ui->pushButton_02->text())
        we_have_a_winner(ui->pushButton_00->text());
    if (ui->pushButton_10->text() == ui->pushButton_11->text() && ui->pushButton_11->text() == ui->pushButton_12->text())
        we_have_a_winner(ui->pushButton_10->text());
    if (ui->pushButton_20->text() ==   ui->pushButton_21->text() && ui->pushButton_21->text() == ui->pushButton_22->text())
        we_have_a_winner(ui->pushButton_20->text());

    /*Check for vertical winner*/
    if (ui->pushButton_00->text() == ui->pushButton_10->text() && ui->pushButton_10->text() == ui->pushButton_20->text())
        we_have_a_winner(ui->pushButton_00->text());
    if (ui->pushButton_01->text() == ui->pushButton_11->text() && ui->pushButton_11->text() == ui->pushButton_21->text())
        we_have_a_winner(ui->pushButton_01->text());
    if (ui->pushButton_02->text() == ui->pushButton_12->text() && ui->pushButton_12->text() == ui->pushButton_22->text())
        we_have_a_winner(ui->pushButton_02->text());

    /*Check for diagonal winner*/
    if (ui->pushButton_00->text() == ui->pushButton_11->text() && ui->pushButton_11->text() == ui->pushButton_22->text())
        we_have_a_winner(ui->pushButton_00->text());
    if (ui->pushButton_20->text() == ui->pushButton_11->text() && ui->pushButton_11->text() == ui->pushButton_02->text())
        we_have_a_winner(ui->pushButton_20->text());

    /*Check for cat's game*/
    if (ui->pushButton_00->isEnabled() == 0 && ui->pushButton_01->isEnabled() == 0 && ui->pushButton_02->isEnabled() == 0)
        if (ui->pushButton_10->isEnabled() == 0 && ui->pushButton_11->isEnabled() == 0 && ui->pushButton_12->isEnabled() == 0)
            if (ui->pushButton_20->isEnabled() == 0 && ui->pushButton_21->isEnabled() == 0 && ui->pushButton_22->isEnabled() == 0)
                if (gameover == false) //Game has not already ended
                    ui->label_topmsg->setText("Cat's game! Meow!");
}

void TicTacToe::we_have_a_winner(QString winner){
    if (winner == "X"){
        ui->label_topmsg->setText("X is the winner!");
        disableGame();
        gameover=1;
    }
    else if (winner == "O"){
        ui->label_topmsg->setText("O is the winner!");
        disableGame();
        gameover=1;

    }
}

void TicTacToe::change_player(){
    if (current_player == "X"){
        current_player = "O";
        qDebug() << "changed player to 0";
        return;
    }
    else if (current_player == "O")
        qDebug() << "changed player to x";
        current_player = "X";
}
void TicTacToe::disableGame(){
    QList<QPushButton *> allPButtons = findChildren<QPushButton *>();
    allPButtons.removeOne(ui->pushButton_exit); //We don't want to disable the exit button
    QPushButton* button;
    foreach(button, allPButtons)
        button->setEnabled(false);

}

void TicTacToe::on_pushButton_00_clicked()
{
    ui->pushButton_00->setText(current_player);
    ui->pushButton_00->setEnabled(false);
    change_player();
    check_board();
}

void TicTacToe::on_pushButton_01_clicked()
{
    ui->pushButton_01->setText(current_player);
    ui->pushButton_01->setEnabled(false);
    change_player();
    check_board();
}

void TicTacToe::on_pushButton_02_clicked()
{
    ui->pushButton_02->setText(current_player);
    ui->pushButton_02->setEnabled(false);
    change_player();
    check_board();
}

void TicTacToe::on_pushButton_10_clicked()
{
    ui->pushButton_10->setText(current_player);
    ui->pushButton_10->setEnabled(false);
    change_player();
    check_board();
}

void TicTacToe::on_pushButton_11_clicked()
{
    ui->pushButton_11->setText(current_player);
    ui->pushButton_11->setEnabled(false);
    change_player();
    check_board();
}

void TicTacToe::on_pushButton_12_clicked()
{
    ui->pushButton_12->setText(current_player);
    ui->pushButton_12->setEnabled(false);
    change_player();
    check_board();
}

void TicTacToe::on_pushButton_20_clicked()
{
    ui->pushButton_20->setText(current_player);
    ui->pushButton_20->setEnabled(false);
    change_player();
    check_board();
}

void TicTacToe::on_pushButton_21_clicked()
{
    ui->pushButton_21->setText(current_player);
    ui->pushButton_21->setEnabled(false);
    change_player();
    check_board();
}

void TicTacToe::on_pushButton_22_clicked()
{
    ui->pushButton_22->setText(current_player);
    ui->pushButton_22->setEnabled(false);
    change_player();
    check_board();
}

void TicTacToe::on_pushButton_exit_clicked()
{
    parentWidget()->show();
    hide();
    deleteLater();
}
