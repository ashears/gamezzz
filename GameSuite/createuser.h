#ifndef CREATEUSER_H
#define CREATEUSER_H

#include <QMainWindow>
//#include "login.h"
#include <sys/types.h>
#include <QObject>
#include "dbmanager.h"
namespace Ui {
class CreateUser;
}

class CreateUser : public QMainWindow
{
    Q_OBJECT

public:
    explicit CreateUser(QWidget *parent = 0);
    ~CreateUser();

private slots:
    void on_pushButton_login_screen_clicked();

    void on_pushButton_submit_clicked();

private:
    Ui::CreateUser *ui;
    DBManager dbmanager;
    //Login *login;
};

#endif // CREATEUSER_H
