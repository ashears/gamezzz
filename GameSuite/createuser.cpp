#include "createuser.h"
#include "ui_createuser.h"
#include "login.h"

CreateUser::CreateUser(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::CreateUser)
{
    ui->setupUi(this);
}

CreateUser::~CreateUser()
{
    delete ui;
}

void CreateUser::on_pushButton_login_screen_clicked()
{
    hide();
    Login *login = new Login(this);
    login->show();
}

void CreateUser::on_pushButton_submit_clicked()
{
    dbmanager.connOpen();
    QString username = ui->lineEdit_username->text();
    QString password = ui->lineEdit_password->text(); //Gets password text
    QSqlQuery qry(dbmanager.mydb);
    qry.prepare("INSERT INTO users (username,password) VALUES ('"+username+"', '"+password+"')");

    if(qry.exec()){
        qry.exec("INSERT INTO highscores (username,2048_score) VALUES ('"+username+"', 0)");
        hide();
        Login *login = new Login(this);
        login->show();
        dbmanager.connClose();
    }
    else{
        ui->statusbar->showMessage(qry.lastError().text());
    }

}
