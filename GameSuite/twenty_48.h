#ifndef TWENTY_48_H
#define TWENTY_48_H

#include <QMainWindow>
#include <QKeyEvent>
#include <map>
#include <cstdlib>
#include <map>
#include <utility>
#include <string>
#include <QtSql>
//#include "login.h" //Causing issues

namespace Ui {
class twenty_48;
}

class twenty_48 : public QMainWindow
{
    Q_OBJECT
public:
    explicit twenty_48(QWidget *parent = 0);
    ~twenty_48();
    void update();
    void leftClick();
    void rightClick();
    void downClick();
    void upClick();
    void addPiece();
    void initializeBoard();
    void printBoard();
    int checkForSpace(int, char);
    void combineLeft();
    void combineRight();
    void combineDown();
    void combineUp();
    QString checkColor(int, int);
    QString current_user;


private slots:



    void on_pushButton_exit_clicked();

private:
    Ui::twenty_48 *ui;
    int **board;
    std::map< int, std::pair<int,int> > open_spaces;
    int size;
    std::map<int, std::pair<int, int> > empty;
    int score;
    QString white;
    QString light_grey;
    QString purple;
    QString violet;
    QString blue;
    QString dark_blue;
    QString light_green;
    QString medium_green;
    QString dark_green;
    QString yellow;
    QString red;
    QString dark_red;
protected:
    void keyPressEvent(QKeyEvent * event);
};

#endif // TWENTY_48_H
