#include "dbmanager.h"
#include <QApplication>

DBManager::DBManager()
{

}

void DBManager::connOpen()
{

    path = QCoreApplication::applicationDirPath() + "/GameSuitedb.db";
    mydb=QSqlDatabase::addDatabase("QSQLITE");
    mydb.setDatabaseName(path);
    if(!mydb.open())
        qDebug()<<"Failed to open the database";
}

void DBManager::connClose() /*Closes connection and commits changes to database*/
{
    mydb.close();
    mydb.removeDatabase(QSqlDatabase::defaultConnection);
}



