#ifndef DBMANAGER_H
#define DBMANAGER_H
#include <QtSql>

class DBManager
{
public:
    void connClose();
    void connOpen();
    QSqlDatabase mydb;
    DBManager();
    QString path;
    //int get2048_highscore(QString username);
};

#endif // DBMANAGER_H
