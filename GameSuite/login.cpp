#include "login.h"
#include "ui_login.h"
#include "QPixmap"
#include "gameselectwindow.h"
Login::Login(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Login)
{
    ui->setupUi(this);
    QPixmap pix(":/resource/img/duck_bath.jpg");//Takes location of image as arg
    int w = ui->label_pic->width();
    int h = ui->label_pic->height();
    ui->label_pic->setPixmap(pix.scaled(w,h,Qt::KeepAspectRatio));
}

Login::~Login()
{
    delete ui;
}

void Login::on_pushButton_login_clicked()
{
    dbmanager.connOpen();
    QString username = ui->lineEdit_username->text(); //Gets username text
    QString password = ui->lineEdit_password->text(); //Gets password text

    QSqlQuery qry;
    qDebug()<<username;
    qDebug()<<password;
    qry.prepare("SELECT * FROM users WHERE username = :username AND password = :password");
    qry.bindValue(":username", username);
    qry.bindValue(":password", password);
    if(qry.exec()){
        int count = 0;
        while(qry.next()){
            count++;
        }
        if(count==1){
            ui->statusBar->showMessage("info correct");
            current_user = username;
            hide(); //Hides login page
            gameSelectWindow = new GameSelectWindow(this);//Creates a new windows where the user can select a game
            gameSelectWindow->current_user = username;
            gameSelectWindow->show();
            ui->statusBar->showMessage("Username and password is correct", 5000);
            dbmanager.connClose();
        }
        if(count>1)
            ui->statusBar->showMessage("Duplicate user info, ERROR");
        if(count<1)
            ui->statusBar->showMessage("Invalid user info");
    }
}

void Login::on_pushButton_createuser_clicked()
{
    hide();
    createUser = new CreateUser(this);
    createUser->show();
}
