#-------------------------------------------------
#
# Project created by QtCreator 2017-02-11T16:12:58
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = GameSuite
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += main.cpp\
        login.cpp \
    gameselectwindow.cpp \
    hangman.cpp \
    createuser.cpp \
    dbmanager.cpp \
    sticks.cpp \
    tictactoe.cpp \
    twenty_48.cpp

HEADERS  += login.h \
    gameselectwindow.h \
    hangman.h \
    createuser.h \
    dbmanager.h \
    sticks.h \
    tictactoe.h \
    twenty_48.h

FORMS    += login.ui \
    gameselectwindow.ui \
    hangman.ui \
    createuser.ui \
    sticks.ui \
    tictactoe.ui \
    twenty_48.ui

RESOURCES += \
    resource.qrc
