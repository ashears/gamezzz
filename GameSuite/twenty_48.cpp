#include "twenty_48.h"
#include "ui_twenty_48.h"

twenty_48::twenty_48(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::twenty_48)
{
    ui->setupUi(this);
    size = 4;
    score = 0;
    empty = std::map<int, std::pair<int,int> >();
    board = new int*[4];
    for(int i = 0; i< 4; i++){
        board[i] = new int[4];
    }
    white = "#ffffff";
    light_grey = "#aeadac";
    purple = "#ae32a0";
    violet = "#6400aa";
    blue = "#14aaff";
    dark_blue= "14148c";
    light_green = "#80c342";
    medium_green = "#328930";
    dark_green = "#006325";
    yellow = "#b40000";
    red = "#f00";
    dark_red = "#b4000";



}

twenty_48::~twenty_48()
{
    delete ui;
}

void twenty_48::initializeBoard(){
    for(int i = 0; i <size; i++){
        for(int j = 0; j < size; j++){
            board[i][j] = 0;
        }
    }
    board[0][0] = 2;
    board[3][3] = 2;
}


void twenty_48::addPiece(){
    empty.clear();
    int arr[] = {2,4};
    int map_size = 0;
    for(int i = 0; i < size; i++){
        for(int j = 0; j < size; j++){
            if(board[i][j] == 0){
                map_size++;
                empty[map_size] = std::make_pair(i,j);
            }
        }
    }
    int num = rand() % map_size + 1;
    int two4 = rand() % 2;
    std::pair<int,int> pear = empty[num];
    int first = pear.first;
    int second = pear.second;
    board[first][second] =  arr[two4];
}

QString twenty_48::checkColor(int i, int j){
    if(board[i][j] == 0){
        return "white";
    }
    if(board[i][j] == 2){
        return light_grey;
    }
    if(board[i][j] == 4){
        return purple;
    }

    if(board[i][j] == 8){
        return violet;
    }


    if(board[i][j] == 16){
        return blue;
    }

    if(board[i][j] == 32){
        return dark_blue;
    }

    if(board[i][j] == 64){
        return light_green;
    }

    if(board[i][j] == 128){
        return medium_green;
    }

    if(board[i][j] == 256){
        return dark_green;
    }

    if(board[i][j] == 512){
        return yellow;
    }

    if(board[i][j] == 1024){
        return red;
    }

    if(board[i][j] == 2048){
        return dark_red;
    }

    else{    return "white";
}
    }


int twenty_48::checkForSpace(int i, char direction){

      switch(direction) {
      //left click
         case 'l' :
          for(int a = 0; a<size; a++){
              if(board[i][a] == 0){
                  return a;
              }
          }
              break;
         //right click
         case 'r' :
          for(int a = 3; a >=0; a--){
              if(board[i][a] == 0){
                  return a;
              }
          }
                break;
        //up click
         case 'u' :
          for(int a = 0; a<size;a++){
              if(board[a][i] == 0){
                  return a;
              }
          }
          break;
          //down click
      case 'd' :
          for(int a = 3; a >= 0; a-- ){
              if(board[a][i] == 0){
                  return a;
              }
          }
          break;
}
      return 10;

}
void twenty_48::combineRight(){
    for(int i = 3; i >= 0; i--){
        for(int j = 3; j >=1; j--){
            if(board[i][j] == board[i][j-1] && board[i][j] > 0){
                int n = board[i][j]*2;
                score += n;
                board[i][j] = n;
                board[i][j-1] = 0;
                rightClick();
            }
        }
    }
}

void twenty_48::combineLeft(){
    for(int i = 0; i < size; i++){
        for(int j = 0; j < 2; j++){
            if(board[i][j] == board[i][j+1] && board[i][j] > 0){
                int n = board[i][j] * 2;
                score += n;
                board[i][j] = n;
                board[i][j+1] = 0;
                leftClick();
            }
        }
    }
}

void twenty_48::downClick(){
    int current_open = 10;
    for(int i = 3; i >=0; i-- ){
        for(int j = 3; j >= 0; j--){
            current_open = checkForSpace(i, 'd');
            if(board[j][i] != 0 && current_open < 9 && j < current_open){
                board[current_open][i] = board[j][i];
                board[j][i] = 0;


            }
        }
    }
}
void twenty_48::combineDown(){
    int n;
    for(int i = 3; i >=0 ; i--){
        for(int j = 3; j <=1; j--){
            if(board[j][i] == board[j-1][i] && board[j][i] > 0){
                n = board[j][i] *2;
                score += n;
                board[j][i] = n;
                board[j-1][i] = 0;
                downClick();
            }

    }
    }
}
void twenty_48::combineUp(){
    for(int i = 0; i < size; i++){
        for(int j=0 ; j< 3; j++){
            if(board[j][i] == board[j+1][i] && board[j][i] > 0){
                int n = board[j][i]*2;
                score += n;
                board[j][i] = n;
                board[j+1][i] = 0;
                upClick();
            }
        }
    }
}

void twenty_48::upClick(){
    int current_open = 10;
    for(int i =0; i < size; i++){
        for(int j = 0; j < size; j++){
            current_open = checkForSpace(i, 'u');
            if(board[j][i] != 0 && current_open < 9 && j > current_open){
                board[current_open][i] = board[j][i];
                board[j][i] = 0;

            }
        }
    }
}

void twenty_48::leftClick(){
    int current_open = 10;
    for(int i = 0; i < size; i++){
        for(int j = 0; j < size; j++){
         current_open = checkForSpace(i,'l');
         int b = board[i][j];
         if(board[i][j] != 0 && current_open < 9 && j > current_open){
             board[i][current_open] = board[i][j];
             board[i][j] = 0;
                }
            }
        }
    }

void twenty_48::rightClick(){
    int current_open = 10;
    for(int i = 0; i< size; i++){
        for(int j = 3; j >=0 ; j--){
            current_open = checkForSpace(i , 'r');
            if(board[i][j] != 0 && current_open < 9 && j < current_open){
                board[i][current_open] = board[i][j];
                board[i][j] = 0;
            }
        }
    }
}

void twenty_48::update(){
    QString color;
    ui->label_3->setStyleSheet("QLabel { background-color : yellow; color: black; }");
    ui->x00->setText(QString::number(board[0][0]));
    color = checkColor(0,0);
    ui->x00->setStyleSheet("QLabel { background-color : " + color + " ; color: black; }");

    ui->x01->setText(QString::number(board[0][1]));
    color = checkColor(0,1);
    ui->x01->setStyleSheet("QLabel { background-color : " + color + " ; color: black; }");

    ui->x02->setText(QString::number(board[0][2]));
    color = checkColor(0,2);
    ui->x02->setStyleSheet("QLabel { background-color : " + color + " ; color: black; }");

    ui->x03->setText(QString::number(board[0][3]));
    color = checkColor(0,3);
    ui->x03->setStyleSheet("QLabel { background-color : " + color + " ; color: black; }");

    ui->x10->setText(QString::number(board[1][0]));
    color = checkColor(1,0);
    ui->x10->setStyleSheet("QLabel { background-color : " + color + " ; color: black; }");

    ui->x11->setText(QString::number(board[1][1]));
    color = checkColor(1,1);
    ui->x11->setStyleSheet("QLabel { background-color : " + color + " ; color: black; }");

    ui->x12->setText(QString::number(board[1][2]));
    color = checkColor(1,2);
    ui->x12->setStyleSheet("QLabel { background-color : " + color + " ; color: black; }");

    ui->x13->setText(QString::number(board[1][3]));
    color = checkColor(1,3);
    ui->x13->setStyleSheet("QLabel { background-color : " + color + " ; color: black; }");

    ui->x20->setText(QString::number(board[2][0]));
    color = checkColor(2,0);
    ui->x20->setStyleSheet("QLabel { background-color : " + color + " ; color: black; }");

    ui->x21->setText(QString::number(board[2][1]));
    color = checkColor(2,1);
    ui->x21->setStyleSheet("QLabel { background-color : " + color + " ; color: black; }");

    ui->x22->setText(QString::number(board[2][2]));
    color = checkColor(2,2);
    ui->x22->setStyleSheet("QLabel { background-color : " + color + " ; color: black; }");


    ui->x23->setText(QString::number(board[2][3]));
    color = checkColor(2,3);
    ui->x23->setStyleSheet("QLabel { background-color : " + color + " ; color: black; }");

    ui->x30->setText(QString::number(board[3][0]));
    color = checkColor(3,0);
    ui->x30->setStyleSheet("QLabel { background-color : " + color + " ; color: black; }");

    ui->x31->setText(QString::number(board[3][1]));
    color = checkColor(3,1);
    ui->x31->setStyleSheet("QLabel { background-color : " + color + " ; color: black; }");

    ui->x32->setText(QString::number(board[3][2]));
    color = checkColor(3,2);
    ui->x32->setStyleSheet("QLabel { background-color : " + color + " ; color: black; }");

    ui->x33->setText(QString::number(board[3][3]));
    color = checkColor(3,3);
    ui->x33->setStyleSheet("QLabel { background-color : " + color + " ; color: black; }");

    ui->score->setText(QString::number(score));
    ui->score->setStyleSheet("QLabel { background-color : #328930; color : blue; }");

}

void twenty_48::keyPressEvent(QKeyEvent *event){
 //   qDebug() << "game knows that i pressed a key";
    if(event->key() == Qt::Key_Left){
        leftClick();
        combineLeft();
        addPiece();
        update();
        //printBoard();
    }
    else if(event->key() == Qt::Key_Right){
        rightClick();
        combineRight();
        addPiece();
        update();
      //  printBoard();

    }else if(event ->key() == Qt::Key_Up){
        upClick();
        combineUp();
        addPiece();
        update();
       // printBoard();

    }
    else if(event->key() == Qt::Key_Down){
        downClick();
        combineDown();
        addPiece();
        update();
        //printBoard();
    }
}
/*
void twenty_48::setHighscore(){
    dbmanager.connOpen();
    QString username = ui->lineEdit_username->text(); //Gets username text
    QString password = ui->lineEdit_password->text(); //Gets password text

    QSqlQuery qry;

    qry.prepare("SELECT * FROM users WHERE username = :username");
    qry.bindValue(":username", username);
    if(qry.exec()){
        int count = 0;
        while(qry.next()){
            count++;
        }
        if(count==1){
            ui->statusBar->showMessage("info correct");
            hide(); //Hides login page
            gameSelectWindow = new GameSelectWindow(this);//Creates a new windows where the user can select a game
            gameSelectWindow->show();
            ui->statusBar->showMessage("Username and password is correct", 5000);
            dbmanager.connClose();
        }
        if(count>1)
            ui->statusBar->showMessage("Duplicate user info, ERROR");
        if(count<1){
            qry.prepare("INSERT INTO highscores (username, 2048_score) VALUES (:username, :score)");
            qry.bindValue(":username", Login::current_user);
        }

}
*/
void twenty_48::on_pushButton_exit_clicked()
{
    //insert or update score
    parentWidget()->show();
    hide();
    deleteLater();

}
