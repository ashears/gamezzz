#ifndef GAMESELECTWINDOW_H
#define GAMESELECTWINDOW_H

#include <QMainWindow>
#include "hangman.h"
#include <sticks.h>
#include <tictactoe.h>
#include "twenty_48.h"
namespace Ui {
class GameSelectWindow;
}

class GameSelectWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit GameSelectWindow(QWidget *parent = 0);
    ~GameSelectWindow();
    QString current_user;

private slots:
    void on_pushButton_3_clicked();
    void on_pushButton_sticks_clicked();
    void on_pushButton_tictactoe_clicked();
    void on_pushButton_clicked();

private:
    Ui::GameSelectWindow *ui;
    Hangman *hangman;
    Sticks *sticks;
    TicTacToe *ttt;
    twenty_48 *game;

};

#endif // GAMESELECTWINDOW_H
